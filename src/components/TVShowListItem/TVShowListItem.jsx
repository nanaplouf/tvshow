import s from "./style.module.css";
import {SMALL_IMG_COVER_BASE_URL} from "../../config";

export function TVShowListItem({tvShow, onClick}){
    return (
        <div onClick={()=> onClick(tvShow)} className={s.container}>
            <img src={SMALL_IMG_COVER_BASE_URL+tvShow.backdrop_path} alt={tvShow.name} className={s.image} />
            <div className={s.title}><p>{tvShow.name}</p></div>
        </div>
    )
}